import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict: true,
    state: {
        products: [
            {name: 'Iphone 13', price: 32000000},
            {name: 'Redmi 10', price: 14000000},
            {name: 'Iphone 12', price: 20000000},
            {name: 'Redmi 11', price: 8000000}
        ]
    },
    getters: {
        saleProducts: (state) => {
            var saleProducts = state.products.map( product => {
                return {
                    name:  '--' + product.name + '--',
                    price: product.price - product.price*0.1,
                };
            });
            return saleProducts;
        }
    },
    mutations: {
        reducePrice: (state, payload) => {
            state.products.forEach( product => {
                product.price -= payload
            });
        }
    },
    actions: {
        reducePrice: (context, payload) => {
            setTimeout(function(){ 
                context.commit('reducePrice', payload);
            }, 700);
        }
    }
});
